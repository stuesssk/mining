#!/usr/local/bin/python

import sys
from random import randint


class Drone:
    moveList = ["EAST", "SOUTH", "WEST", "NORTH"]

    def __init__(self):
        """Method to intialize the Drone"""
        self.lz = [0, 0]
        self.moves = 0
        self.index = 0
        self.timesToMove = 2
        self.count = 1
        self.spiral = True
        self.prevMove = ["", ""]
        self.resources = 0
        self.nextMove = None
        self.ticks = 0
        self.reverse = False
        self.beconRecall = False
        self.deployed = False
        self.closing = False
        self.context = None
        self.traverseAcid = True
        self.inAcid = False
        self.acidHits = 0

    def zeroAttributes(self):
        """Function used to zero drone attributes upon their return"""
        self.lz = [0, 0]
        self.deployed = False
        self.beconRecall = False
        self.closing = False
        self.prevMove = ["", ""]

    def unstuckRecall(self, context, deltaX, deltaY):
        """Fucntion used to lead the drone away from a location where the
           recall fucntion is not allowing drone to adnvace. An imaginary
           LZ is set in the opposite quadrant as the true LZ, relative to the
           drone and priority is given to y-axis travel over x-axis.
        """
        # Set imaginary LZ to be in exact opposite grid quadrant as the
        # real LZ
        if self.timesToMove == 0:
            self.reverse = False
            self.prevMove[0] = "CENTER"
            return "CENTER"
        tempx = -deltaX
        tempy = -deltaY
        self.timesToMove -= 1
        # While attempting to get unstuck, priority will be given to
        # y-axis movement then x-axis movement (Reverse of normal moves)
        if deltaY > 0 and context.south in "* _":
            self.prevMove[0] = Drone.moveList[1]
            return Drone.moveList[1]
        elif deltaY < 0 and context.north in "* _":
            self.prevMove[0] = Drone.moveList[3]
            return Drone.moveList[3]
        elif deltaX > 0 and context.west in "* _":
            self.prevMove[0] = Drone.moveList[2]
            return Drone.moveList[2]
        elif deltaX < 0 and context.east in "* _":
            self.prevMove[0] = Drone.moveList[0]
            return Drone.moveList[0]
        # If there is no clear path back to LZ, risk the acid
        elif deltaY > 0 and context.south in "~":
            self.prevMove[0] = Drone.moveList[1]
            return Drone.moveList[1]
        elif deltaY < 0 and context.north in "~":
            self.prevMove[0] = Drone.moveList[3]
            return Drone.moveList[3]
        elif deltaX > 0 and context.west in "~":
            self.prevMove[0] = Drone.moveList[2]
            return Drone.moveList[2]
        elif deltaX < 0 and context.east in "~":
            self.prevMove[0] = Drone.moveList[0]
            return Drone.moveList[0]
        # If unable to go in reverse than return to going to real LZ
        self.reverse = False
        self.prevMove[0] = "CENTER"
        return "CENTER"

    def recall(self, context, deltaX, deltaY):
        """Fucntion used to return the drone to the Landig Zone. Priority
           is given to x-axis travel over y-axis travel
        """
        # If deltas are zero you are on the LZ
        if deltaX == 0 and deltaY == 0:
            self.beconRecall = True
            return "CENTER"

        # First check if there is a path that wont injure drone
        # on path back to LZ
        if deltaX > 0 and context.west in "* _":
            self.prevMove[0] = Drone.moveList[2]
            if context.west in "_":
                self.beconRecall = True
            return Drone.moveList[2]
        elif deltaX < 0 and context.east in "* _":
            self.prevMove[0] = Drone.moveList[0]
            if context.east in "_":
                self.beconRecall = True
            return Drone.moveList[0]
        elif deltaY > 0 and context.south in "* _":
            self.prevMove[0] = Drone.moveList[1]
            if context.south in "_":
                self.beconRecall = True
            return Drone.moveList[1]
        elif deltaY < 0 and context.north in "* _":
            self.prevMove[0] = Drone.moveList[3]
            if context.north in "_":
                self.beconRecall = True
            return Drone.moveList[3]
        # If there is no clear path back to LZ, risk the acid
        elif deltaX > 0 and context.west in "~":
            self.prevMove[0] = Drone.moveList[2]
            return Drone.moveList[2]
        elif deltaX < 0 and context.east in "~":
            self.prevMove[0] = Drone.moveList[0]
            return Drone.moveList[0]
        elif deltaY > 0 and context.south in "~":
            self.prevMove[0] = Drone.moveList[1]
            return Drone.moveList[1]
        elif deltaY < 0 and context.north in "~":
            self.prevMove[0] = Drone.moveList[3]
            return Drone.moveList[3]

        # If center is returned while traversing back to LZ, then
        # the drone must be stuck, set the reverse to true and calculate
        # the number of steps to take the reverse path back
        self.prevMove[0] = "CENTER"
        self.reverse = True
        # Max moves left is 2.5 times distance to LZ, backtracking by half the
        # distance to the LZ, brings the drone in at exactly 2 times that dist
        self.timesToMove = (abs(deltaX) + abs(deltaY)) // 2
        return "CENTER"

    def locateLZ(self):
        """Method to determine the coordinates of the landing zone"""
        try:
            if self.lz[0] == 0 and self.lz[1] == 0:
                if self.context.north in '_Z':
                    self.lz[0] = self.context.x
                    self.lz[1] = self.context.y + 1
                elif self.context.south in '_Z':
                    self.lz[0] = self.context.x
                    self.lz[1] = self.context.y - 1
                elif self.context.east in '_Z':
                    self.lz[0] = self.context.x + 1
                    self.lz[1] = self.context.y
                elif self.context.west in '_Z':
                    self.lz[0] = self.context.x - 1
                    self.lz[1] = self.context.y
        except AttributeError:
            pass

    def checkAround(self, context):
        """Method to check for minerals in the spaces next to the drone"""
        for cord in Drone.moveList:
            if getattr(context, cord.lower()) in "*":
                return cord
        return None

    def calcSpiral(self):
        """Method to keep track of the steps for moving in a spiral path"""
        if self.spiral:
            if self.moves == self.timesToMove and self.count is not 2:
                self.moves = 0
                self.index = (self.index + 1) % 4
                self.count += 1
            elif self.moves == self.timesToMove and self.count == 2:
                self.index = (self.index + 1) % 4
                self.count = 1
                self.moves = 0
                self.timesToMove += 2
            self.moves += 1

    def recallDirective(self):
        """Method to check if it is time for the drone to return to the
           landing zone for extraction"""
        try:
            deltaX = self.context.x - self.lz[0]
            deltaY = self.context.y - self.lz[1]
            distance = 2.25*(abs(deltaX)+abs(deltaY))
            if distance >= self.ticks:
                self.closing = True
        except AttributeError:
            pass

    def resetSpiral(self):
        """Method to allow the drone to resume a spiral search pattern"""
        if self.moves == 0:
            self.spiral = True
            self.index = 0

    def spiralMove(self):
        """Method to  determine the drones next move, when searching in
           spiral search pattern
        """
        self.nextMove = Drone.moveList[(self.index)]

    def roombaMove(self):
        """Methos to calculate the next move when the drone is moving in a
           Roomba® like search pattern
        """
        self.nextMove = self.prevMove[0]
        if self.moves <= 0 or self.prevMove[1] == "CENTER":
            self.moves = randint(4, 40)
            self.nextMove = Drone.moveList[randint(0, 3)]
            self.prevMove[1] = ""
        self.moves -= 1

    def acid(self, context):
        """Method to check if the drone is in acid and if the drone has
           reached its allowed number of acid hits while in a search mode
        """
        if self.inAcid:
            self.acidHits += 3
        if self.acidHits >= 15:
            self.traverseAcid = False

    def move(self, context):
        """Method to control the movement of each drone"""
        # If the becon is being flashed to the Overlord or drone is not
        # deployed, don't move
        if not self.deployed or self.beconRecall:
            return "CENTER"

        # Checking if on acid and if the drone is still allowed to traverseAcid
        # the acid
        self.acid(context)

        # Saving your context so Overlord can have access to
        # run calculations during its turn if time allows
        self.context = context

        # Grab any minerals that you see, even if not in directon
        # you are intending to go
        newMove = self.checkAround(context)
        if newMove is not None and self.ticks > 10:
            # Keeps drone on track when Overlord is calulating spiral moves
            # in advance
            if self.spiral:
                self.moves -= 1
            return newMove

        deltaX = context.x - self.lz[0]
        deltaY = context.y - self.lz[1]
        # Make sure drone is not stuck in a corner trying to return to LZ
        if self.reverse:
            return self.unstuckRecall(context, deltaX, deltaY)
        # If it's close to closing time, return to the LZ
        if self.closing:
            return self.recall(context, deltaX, deltaY)

        # If the Overlord did not have time to run these calculations
        # the drone must run them himself
        if not self.nextMove:
            # If overlord has not calulated, caculate it for itself
            if self.spiral:
                # Using spiral search method
                self.spiralMove()
            else:
                # The spiral has broken down time to move in phase two of
                # of Roomba movement
                self.roombaMove()

        # Return the actual movement you wish
        self.prevMove[0] = self.nextMove
        droneMove = self.nextMove
        self.nextMove = None
        for cord in Drone.moveList:
            stringTest = getattr(context, cord.lower())
            self.prevMove[0] = cord
            self.inAcid = False
            if droneMove == cord and stringTest in '* ':
                return cord
            if not self.traverseAcid and self.inAcid and stringTest in " ":
                return cord
            # Allow the traversal of acid if the drone still has enough health
            # to do it safely and it is the only option
            if self.traverseAcid:
                if droneMove == cord and stringTest in '~':
                    self.inAcid = True
                    return cord

        # No moves to make from the current set of instructions. If spiraling
        # set to false and hang tight for the tick.
        self.spiral = False
        self.prevMove[1] = "CENTER"
        return 'CENTER'


class Overlord:

    def __init__(self, ticks):
        """Method to intialize the Overlord"""
        self.maps = {}
        self.zerg = {}
        self.ticks = ticks

        for _ in range(6):
            z = Drone()
            self.zerg[id(z)] = z
            z.id = id(z)

    def add_map(self, map_id, summary):
        """Method to register an identifier fro a map with the Overlord
           along with a summary of the map
        """
        self.maps[map_id] = summary

    def canDeploy(self):
        """Method to determine if a drone is able to be deployed on a map.
           Used to ensure drones are not deployed on top of each other, while
           one is still located on the landing zone
        """
        countLz = 0
        countDeploy = 0
        # Count the number of LZs calulated and number of drones deployed
        for droneId in self.zerg:
            drone = self.zerg.get(droneId)
            if drone.lz[0] != 0 and drone.lz[1] != 0:
                countLz += 1
            if drone.deployed:
                countDeploy += 1
        # If less than 3 deployed, the first three have not been deployed
        if countDeploy < 3:
            return True
        # If LZ is calulated, drone can not be on the LZ
        # will only be dropping 2 per map, as long as the initial 3 drones
        # have been deployed the last three can be deployed in order.
        elif countDeploy >= 3 and countLz >= 3:
            return True
        return False

    def action(self):
        """Fuction to determine the action the Overlord will take each turn"""
        self.ticks -= 1
        # Functions that must be executed on each Overlord turn
        for droneId in self.zerg:
            drone = self.zerg.get(droneId)
            drone.ticks = self.ticks
            drone.calcSpiral()
            drone.locateLZ()
            drone.recallDirective()
            if not drone.deployed and self.ticks > 15:
                # Need to ensure we won't be deploying a drone onto another
                if self.canDeploy():
                    if self.ticks < 997:
                        # First drone on the map will have already
                        # covered the spiral
                        drone.spiral = False
                    drone.deployed = True
                    mapnum = self.ticks % 3
                    return 'DEPLOY {} {}'.format(droneId, mapnum)
            if drone.beconRecall:
                drone.zeroAttributes()
                return 'RETURN {}'.format(droneId)

        # Calculations that are not priority, but would take some calculation
        # time away from the drones
        for droneId in self.zerg:
            drone = self.zerg.get(droneId)
            drone.resetSpiral()
            if drone.spiral:
                drone.spiralMove()
            else:
                drone.roombaMove()

        return "WAIT"
